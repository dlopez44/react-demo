function Cart(props) {
  const cart = props.cart;
  const setCart = props.setCart;
  function increment(cartItemName) {
    const newCart = [...cart]; // javascript spread operator
    const index = newCart.findIndex(function (cartItem) {
      return cartItem.name === cartItemName;
    });
    const cartItem = newCart[index];
    cartItem.quantity++;
    setCart(newCart);
  }
  function decrement(cartItemName) {
    const newCart = [...cart]; // javascript spread operator
    const index = newCart.findIndex(function (cartItem) {
      return cartItem.name === cartItemName;
    });
    const cartItem = newCart[index];
    if (cartItem.quantity === 1) {
      newCart.splice(index, 1);
    } else cartItem.quantity--;
    setCart(newCart);
  }
  return (
    <div>
      <h2>
        Cart (
        {cart.reduce(function (total, cartItem) {
          const currentTotal = total + cartItem.quantity;
          return currentTotal;
        }, 0)}
        )
      </h2>

      {cart.length === 0 ? (
        <p>your cart is empty</p>
      ) : (
        <ul>
          {cart.map(function (cartItem) {
            return (
              <li>
                <h2>{cartItem.name}</h2>
                <h3>{cartItem.price}</h3>
                <h3>
                  <button
                    type="button"
                    onClick={function () {
                      decrement(cartItem.name);
                    }}
                  >
                    -
                  </button>
                  {cartItem.quantity}
                  <button
                    type="button"
                    onClick={function () {
                      increment(cartItem.name);
                    }}
                  >
                    +
                  </button>
                </h3>
              </li>
            );
          })}
        </ul>
      )}
    </div>
  );
}

export default Cart;
