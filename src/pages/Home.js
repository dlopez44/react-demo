import { Link } from "react-router-dom";

import Page from "../components/Page";
import categories from "../lib/categories";

function HomePage() {
  return (
    <Page title="home page">
      <h2>categories</h2>
      <ul>
        {Object.entries(categories).map(function ([key, value]) {
          return (
            <li>
              <Link to={`/categories/${value.id}`}>{value.title}</Link>
            </li>
          );
        })}
      </ul>
    </Page>
  );
}

export default HomePage;
