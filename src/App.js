import { useState } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import logo from "./logo.svg";
import "./App.css";
import HomePage from "./pages/Home";
import AboutPage from "./pages/About";
import CategoryPage from "./pages/Category";
import CartPage from "./pages/Cart";

function Header(props) {
  const cartTotal = props.cartTotal;
  const [results, setResults] = useState([]);
  const [form, setForm] = useState({
    search: "",
  });
  function handleChange(event) {
    const { name, value } = event.target;
    setForm({
      ...form,
      [name]: value,
    });
  }
  return (
    <header className="App-header">
      <div className="flex">
        <img src={logo} className="App-logo" alt="logo" />
        <nav>
          <ul className="flex">
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/about">About</Link>
            </li>
            <li>
              <Link to="/cart">Cart ({cartTotal})</Link>
            </li>
          </ul>
        </nav>
      </div>
      <div className="search flex">
        <input
          type="text"
          placeholder="search products here"
          onChange={handleChange}
          name="search"
          value={form.search}
        />
        <button
          type="button"
          onClick={async function () {
            const newResults = await fetch(
              `http://localhost:1234/api/search/${form.search}`
            ).then((res) => res.json());
            setResults(newResults);
          }}
        >
          search
        </button>
        <button
          type="button"
          onClick={function () {
            setResults([]);
            setForm({
              search: "",
            });
          }}
        >
          reset
        </button>
      </div>
      {results.length > 0 && (
        <ul>
          {results.map((result) => (
            <li>{result}</li>
          ))}
        </ul>
      )}
    </header>
  );
}

function App() {
  const [cart, setCart] = useState([]);
  const cartTotal = cart.reduce(function (total, cartItem) {
    const currentTotal = total + cartItem.quantity;
    return currentTotal;
  }, 0);
  return (
    <div className="App">
      <Router>
        <Header cartTotal={cartTotal} />
        <Switch>
          <Route path="/about">
            <AboutPage />
          </Route>
          {/* <Route path="/supplements">
            <SupplementsPage cart={cart} setCart={setCart} />
          </Route> */}
          <Route path="/categories/:categoryId">
            <CategoryPage cart={cart} setCart={setCart} />
          </Route>
          <Route path="/cart">
            <CartPage cart={cart} setCart={setCart} />
          </Route>
          <Route path="/">
            <HomePage />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
