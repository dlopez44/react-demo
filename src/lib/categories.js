const categories = {
  supplements: {
    id: "supplements",
    title: "Supplements",
    products: [
      {
        name:
          "Probiotic Acidophilus 250 Million Organisms, 240 Quick Release Capsules",
        price: 6.89,
      },
      {
        name: "Inulin Prebiotic FOS Powder (Organic), 15 oz (425 g) Bottle",
        price: 11.49,
      },
    ],
  },
  vitamins: {
    id: "vitamins",
    title: "Vitamins",
    products: [
      {
        name: "Mega Multiple for Men 50 Plus, 100 Coated Caplets",
        price: 10.59,
      },
      {
        name: "Woman's Mega Multi 50 Plus, 100 Coated Caplets",
        price: 10.59,
      },
    ],
  },
  herbs: {
    id: "herbs",
    title: "Herbal Supplements",
    products: [
      {
        name: "Elderberry Sambucus, 1000 mg, 180 Quick Release Capsules",
        price: 6.39,
      },
      {
        name:
          "Sambucus Black Elderberry Extract, 4250 mg, 8 fl oz (237 mL) Bottle",
        price: 7.99,
      },
    ],
  },
  "essential-oils": {
    id: "essential-oils",
    title: "Essential Oils",
    products: [
      {
        name: "Japanese Peppermint Oil, 1/2 oz (15 mL) Dropper Bottle",
        price: 2.99,
      },
      {
        name:
          "Peppermint Pure Essential Oil (GC/MS Tested), 1/2 fl oz (15 mL) Dropper Bottle",
        price: 3.99,
      },
    ],
  },
};

export default categories;
