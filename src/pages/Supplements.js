import { useState } from "react";

import Page from "../components/Page";

const products = [
  {
    name:
      "Probiotic Acidophilus 250 Million Organisms, 240 Quick Release Capsules",
    price: 6.89,
  },
  {
    name:
      "Women’s Probiotic 8 Strains 5 Billion Organisms plus Cranberry, 90 Vegetarian Capsules",
    price: 6.89,
  },
];

function Supplements(props) {
  const [count, setCount] = useState(0);
  const cart = props.cart;
  const setCart = props.setCart;
  return (
    <Page title="Supplements">
      <div>
        <h2>
          Cart (
          {cart.reduce(function (total, cartItem) {
            const currentTotal = total + cartItem.quantity;
            return currentTotal;
          }, 0)}
          )
        </h2>
        <ul>
          {cart.map(function (cartItem) {
            return (
              <li>
                <h2>{cartItem.name}</h2>
                <h3>{cartItem.price}</h3>
                <h3>{cartItem.quantity}</h3>
              </li>
            );
          })}
        </ul>
      </div>
      <h2>Products</h2>
      <ul>
        {products.map(function (product) {
          return (
            <li>
              <h2>{product.name}</h2>
              <h3>{product.price}</h3>
              <button
                onClick={function () {
                  const newCart = [...cart]; // javascript spread operator
                  const index = newCart.findIndex(function (cartItem) {
                    return cartItem.name === product.name;
                  });
                  if (index === -1) {
                    newCart.push({
                      name: product.name,
                      price: product.price,
                      quantity: 1,
                    });
                  } else {
                    const cartItem = newCart[index];
                    cartItem.quantity++;
                    // const newCartItem = {
                    //   name: cartItem.name,
                    //   price: cartItem.price,
                    //   quantity: cartItem.quantity + 1,
                    // };
                    // newCart[index] = newCartItem;
                  }
                  setCart(newCart);
                  setCount(count + 1);
                  // alert(count);
                }}
              >
                Add to Cart
              </button>
            </li>
          );
        })}
      </ul>
    </Page>
  );
}

export default Supplements;
