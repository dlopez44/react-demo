function Page(props) {
  const title = props.title;
  const children = props.children;
  return (
    <div className="page">
      <h1>{title}</h1>
      {children}
    </div>
  );
}

export default Page;
